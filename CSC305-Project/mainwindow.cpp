#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("2B Travel");

    /* Temperature Spin Box */
    ui->minSpinBox->setRange(-30,100);
    ui->maxSpinBox->setRange(-20,110);

    /* Water Combo Box */
    ui->waterBox->addItem("Ocean");
    ui->waterBox->addItem("Lake");
    ui->waterBox->addItem("River");
    ui->waterBox->addItem("No water");

    /* Month Combo Box */
    ui->monthBox->addItem("January");
    ui->monthBox->addItem("February");
    ui->monthBox->addItem("March");
    ui->monthBox->addItem("April");
    ui->monthBox->addItem("May");
    ui->monthBox->addItem("June");
    ui->monthBox->addItem("July");
    ui->monthBox->addItem("August");
    ui->monthBox->addItem("September");
    ui->monthBox->addItem("October");
    ui->monthBox->addItem("Novermber");
    ui->monthBox->addItem("December");

    /* Placeholder Map */
    ui->mapPlaceholder->setStyleSheet("border-image:url(:/new/prefix1/CroppedNA.jpg);");
}

MainWindow::~MainWindow()
{
    delete ui;
}

